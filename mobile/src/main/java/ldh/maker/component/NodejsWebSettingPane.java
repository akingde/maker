package ldh.maker.component;

import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh123 on 2018/5/6.
 */
public class NodejsWebSettingPane extends SettingPane {

    private NodejsWebTableUi nodejsWebTableUi;

    public NodejsWebSettingPane(TreeItem<TreeNode> treeItem, String dbName) {
        super(treeItem, dbName);

        buildTableUiTab();

        if (this.getTabs().size() > 0) {
            Tab tab = this.getTabs().get(0);
            if (tab.getContent() instanceof BaseSettingTabPane) {
                ((BaseSettingTabPane) tab.getContent()).show();
            }
        }
    }

    private void buildTableUiTab() {
        nodejsWebTableUi = new NodejsWebTableUi(treeItem, dbName);
        Tab tab = createTab("Table设置", nodejsWebTableUi);
        tab.selectedProperty().addListener((b,o,n)->{
            if (tab.isSelected()) {
                nodejsWebTableUi .show();
            }
        });
    }

    @Override
    protected PackagePane buildPackagePaneTab() {
        return null;
    }
}
