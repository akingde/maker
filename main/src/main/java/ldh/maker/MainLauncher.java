package ldh.maker;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import ldh.fx.transition.FadeInRightBigTransition;
import ldh.maker.page.LoadingPage;

public class MainLauncher extends Application {

    public static Stage STAGE = null;

    @Override
    public void start(Stage primaryStage) throws Exception {
        STAGE = primaryStage;

        primaryStage.setOnCloseRequest((e)->{
            Platform.exit();
            System.exit(0);
        });
        showLoadingPage(primaryStage, null);
    }

    private void showLoadingPage(Stage primaryStage, Node node) {
        Stage stage = new Stage();
        stage.initOwner(primaryStage);
        LoadingPage loadingPage = new LoadingPage(650, 350);
        loadingPage.setStage(stage);
        Scene scene = new Scene(loadingPage);
        scene.getStylesheets().add("/styles/bootstrapfx.css");
        scene.getStylesheets().add(ServerMain.class.getResource("/styles/jfoenix-components.css").toExternalForm());
        stage.setScene(scene);
        scene.setFill(null);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.centerOnScreen();

        loadingPage.setOpacity(0);
        stage.show();

        FadeInRightBigTransition fadeInRightTransition = new FadeInRightBigTransition(loadingPage);
        fadeInRightTransition.setDelay(new Duration(800));
        fadeInRightTransition.playFromStart();
    }

    public static void main(String[] args) {
        launch(args);
    }
}