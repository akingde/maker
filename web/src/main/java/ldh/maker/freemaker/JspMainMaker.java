package ldh.maker.freemaker;


import ldh.maker.database.TableInfo;

public class JspMainMaker extends FreeMarkerMaker<JspMainMaker> {
	
	private TableInfo tableInfo;
	private boolean isShiro = false;
	
	public JspMainMaker tableInfo(TableInfo tableInfo) {
		this.tableInfo = tableInfo;
		return this;
	}
	
	public JspMainMaker make() {
		data();
		out(ftl, data);
		
		return this;
	}

	public JspMainMaker shiro(boolean isShiro) {
		this.isShiro = isShiro;
		return this;
	}

	public void data() {
		check();
		data.put("tableInfo", tableInfo);
		data.put("shiro", isShiro);
//		data.put("module", MakerConfig.getInstance().getParam().getModule());
	}
	
	
	protected void check() {
		if (tableInfo == null) {
			throw new NullPointerException("tableInfo must not be null");
		}
//		fileName = "main.jsp";
		super.check();
		
	}
}
