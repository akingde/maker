package ldh.maker.freemaker;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ldh on 2018/2/26.
 */
public class FreemarkerConfigMaker extends FreeMarkerMaker<FreemarkerConfigMaker> {

    protected String basePackage;
    protected String author;


    public FreemarkerConfigMaker() {

    }

    public FreemarkerConfigMaker basePackage(String basePackage) {
        this.basePackage = basePackage;
        return this;
    }

    public FreemarkerConfigMaker author(String author) {
        this.author = author;
        return this;
    }

    public void data() {

        fileName = "FreemarkerConfig.java";
        data.put("fileName", fileName);
        data.put("basePackage", basePackage);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str=sdf.format(new Date());
        data.put("Author",author);
        data.put("DATE", str);
        check();
    }

    public void check() {
        super.check();
    }

    @Override
    public FreemarkerConfigMaker make() {
        data();
        out(ftl, data);

        return this;
    }
}
