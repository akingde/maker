<#macro pagination pageNo pageTotal total action formId pageSize=10 method="get" pageView=10>

<#assign first=PaginationUtil.getFirstPage(pageNo, pageTotal, pageView)/>
<#assign end=PaginationUtil.getEndPage(pageNo, pageTotal, pageView)>

<div class="pagination-tool">
    <form name="${formId }" id="${formId }" action="${action }" method="${method}">
        <input type="hidden" name="pageNo" id="${formId }_pageNo" value="${pageNo }"/>
		<#nested>
        <div class="message">共<i class="blue">${total }</i>条记录，当前显示第&nbsp;<i class="blue">${pageNo }&nbsp;</i>页</div>
        <nav aria-label="Page navigation">
            <ul class="pagination">
                <li class="page-item"><a href="javascript:$paginationFirst('${formId }')" class="page-link">首页</a></li>
                <#list first..end as page>
                    <li class="page-item <#if pageNo == page>active</#if>"><a href="javascript:$pagination('${formId }', ${page })" class="page-link">${page }</a></li>
                </#list>

                <li class="page-item"><a href="javascript:$pagination('${formId }', ${pageTotal })" class="page-link">尾页</a></li>

                <li class="page-item"><span class="page-size">每页数量
				<select name="pageSize" id="${formId }_pageSize" onChange="$paginationSelect('${formId }')">
					<option value="${pageSize }">请选择</option>
					<option value="5" <#if pageSize == 5>selected</#if>>5</option>
                    <option value="10" <#if pageSize == 10>selected</#if>>10</option>
                    <option value="20" <#if pageSize == 20>selected</#if>>20</option>
                    <option value="30" <#if pageSize == 30>selected</#if>>30</option>
				</select></span>
                </li>
            </ul>
        </nav>
    </form>
</div>

</#macro>



