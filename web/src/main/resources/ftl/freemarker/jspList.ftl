${r'<#'}assign ctx=springMacroRequestContext.contextPath />
<${r'#'}include "/macro/publicMacro.ftl">
<${r'#'}import "/macro/pagination.ftl" as Pagination>

<${r'@'}header title="${util.comment(table)}列表">
    <link href="${r'${ctx}'}/resource/common/css/pagination.css" rel="stylesheet">
    <link href="${r'${'}ctx}/resource/frame/datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet">
</${r'@'}header>

<${r'@'}body>
    <h2 class="module-title">${util.comment(table)}列表</h2>

    <div class="alert alert-primary" role="alert">
        <form class="form-inline form-margin" action="${r'${'}ctx}/${util.firstLower(table.javaName)}/list">
        <#list table.columnList as column>
            <#if column.create>
            <#if column.foreign>
            <div class="form-group mx-sm-3 mb-2">
                <label for="${column.property}" class="ml-2">${util.comment(column)}：</label>
                <input type="text" class="form-control" name="${column.property}.${column.foreignKey.foreignColumn.property}" id="${column.property}-${column.foreignKey.foreignColumn.property}" aria-describedby="${util.comment(column)}" placeholder="${util.comment(column)}" value="${r'${'}${util.firstLower(table.javaName)}.${column.property}.${column.foreignKey.foreignTable.columnList[0].property}${r'}'}">
            </div>
            <#elseif util.isPrimaryKey(table, column)>
            <#elseif util.isDate(column) && util.comment(column)?? && util.comment(column) != ''>
            <div class="form-group mx-sm-3 mb-2">
                <label for="start${util.firstUpper(column.property)}" class="ml-2">开始${util.comment(column)}：</label>
                <input type="text" class="form-control" name="start${util.firstUpper(column.property)}" id="start${util.firstUpper(column.property)}" aria-describedby="${util.comment(column)}" placeholder="${util.comment(column)}" value="${r'${('}${util.firstLower(table.javaName)}.start${util.firstUpper(column.property)}?string('yyyy-MM-dd hh:mm:ss')${r')!}'}">
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
            <div class="form-group mx-sm-3 mb-2">
                <label for="end${util.firstUpper(column.property)}" class="ml-2">结束${util.comment(column)}：</label>
                <input type="text" class="form-control" name="end${util.firstUpper(column.property)}" id="end${util.firstUpper(column.property)}" aria-describedby="${util.comment(column)}" placeholder="${util.comment(column)}" value="${r'${('}${util.firstLower(table.javaName)}.end${util.firstUpper(column.property)}?string('yyyy-MM-dd hh:mm:ss')${r')!}'}">
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
            <#elseif util.isEnum(column)>
            <div class="form-group mx-sm-3 mb-2">
                <label for="${column.property}" class="ml-2">${util.comment(column)}：</label>
                <select class="form-control" name="${column.property}" id="${column.property}">
                    <option value="" >请选择</option>
                    <${r'#'}list statusValues as status>
                        <option value="${r'${'}${column.property}.value}" <${r'#'}if ${column.property} == ${util.firstLower(table.javaName)}.${column.property}>selected</${r'#'}if>>${r'${'}${column.property}.desc}</option>
                    </${r'#'}list>
                </select>
            </div>
            <#elseif util.comment(column)?? && util.comment(column) != ''>
            <div class="form-group mb-2">
                <label for="${column.property}" class="ml-2">${util.comment(column)}：</label>
                <input type="text" class="form-control" id="${column.property}" name="${column.property}" value="${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'}'}">
            </div>
            <#else>
                <!--  do nothing -->
            </#if>
            </#if>
        </#list>

            <div class="form-group mx-sm-3 mb-2">
                <button type="submit" class="btn btn-primary">查询</button>
            </div>

            <#if shiro>${r'<@'}shiro.hasPermission name="${util.firstLower(table.javaName)}:toAdd"></#if>
            <div class="form-group mx-sm-3 mb-2" style="float:right">
                <span style="float:left"><a href="${r'${ctx}'}/${util.firstLower(table.javaName)}/toAdd">添加数据</a></span>
            </div>
            <#if shiro>${r'</@'}shiro.hasPermission></#if>
        </form>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <#list table.columnList as column>
                <#if column.create>
                <th>${util.comment(column)}</th>
                </#if>
            </#list>
                <th>操作</th>
            </thead>
            <tbody>
            <${r'#'}list ${util.firstLower(table.javaName)}s.beans as ${util.firstLower(table.javaName)}>
                <tr>
                <#list table.columnList as column>
                <#if column.create>
                <#if column.foreign>
                    <th<#if column.width??> width="${column.width}"</#if>>${r'${'}${util.firstLower(table.javaName)}.${column.property}.${column.foreignKey.foreignTable.columnList[0].property}${r'!}'}</th>
                <#elseif util.isDate(column)>
                    <th<#if column.width??> width="${column.width}"</#if>>${r'${('}${util.firstLower(table.javaName)}.${column.property}?string('yyyy-MM-dd hh:mm:ss')${r')!}'} </th>
                <#elseif util.isNumber(column)>
                    <th<#if column.width??> width="${column.width}"</#if>>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'!}'}</th>
                <#elseif util.isEnum(column)>
                    <th<#if column.width??> width="${column.width}"</#if>>${r'${'}${util.firstLower(table.javaName)}.${column.property}.desc${r'!}'}</th>
                <#else>
                    <th<#if column.width??> width="${column.width}"</#if>>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'!}'}</th>
                </#if>
                </#if>
                </#list>
                    <th><#if shiro>${r'<@'}shiro.hasPermission name="${util.firstLower(table.javaName)}:view"></#if><a class="btn btn-primary btn-sm" href="${r'${'}ctx}/${util.firstLower(table.javaName)}/view/${r'${'}${util.firstLower(table.javaName)}.${table.primaryKey.column.property}${r'?c}'}" role="button" aria-pressed="true">详情</a><#if shiro>${r'</'}@shiro.hasPermission></#if>
                        <#if shiro>${r'<@'}shiro.hasPermission name="${util.firstLower(table.javaName)}:edit"></#if><a class="btn btn-info btn-sm" href="${r'${'}ctx}/${util.firstLower(table.javaName)}/toEdit/${r'${'}${util.firstLower(table.javaName)}.${table.primaryKey.column.property}${r'?c}'}" role="button" aria-pressed="true">编辑</a><#if shiro>${r'</'}@shiro.hasPermission></#if>
                    </th>
                </tr>
            </${r'#'}list>
            </tbody>
        </table>

        <${r'@'}Pagination.pagination formId="${util.firstLower(table.javaName)}Form"
            pageNo=${util.firstLower(table.javaName)}s.pageNo
            pageTotal=${util.firstLower(table.javaName)}s.pageTotal
            total=${util.firstLower(table.javaName)}s.total
            pageSize=${util.firstLower(table.javaName)}s.pageSize
            action="${r'${'}ctx}/${util.firstLower(table.javaName)}/list">
        </${r'@'}Pagination.pagination>
    </div>
</${r'@'}body>

<${r'@'}footer>
    <script src="${r'${'}ctx}/resource/frame/datetimepicker/jquery.datetimepicker.full.min.js"></script>

    <script type="text/javascript">
        $(function () {
        <#list table.columnList as column>
            <#if util.isDate(column)>
                $('#start${util.firstUpper(column.property)}').datetimepicker({
                    format:'Y-m-d H:i:s',
                    lang:'zh'});
                $('#end${util.firstUpper(column.property)}').datetimepicker({
                    format:'Y-m-d H:i:s',
                    lang:'zh'});
            </#if>
        </#list>
        });
    </script>

    <script src="${r'${ctx}'}/resource/common/js/pagination.js"></script>
    <script src="${r'${ctx}'}/resource/common/js/jquery.validate.min.js"></script>
</${r'@'}footer>