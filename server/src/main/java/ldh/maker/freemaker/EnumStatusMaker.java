package ldh.maker.freemaker;

import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.EnumData;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by ldh on 2017/4/19.
 */
public class EnumStatusMaker extends BeanMaker<EnumStatusMaker> {

    protected String name;
    protected Class type;
    protected String choiceType;
    protected String columnName;
    protected String tableName;
    protected Map<String, EnumData> values = new LinkedHashMap<>();

    public EnumStatusMaker type(Class type) {
        this.type = type;
        return this;
    }

    public EnumStatusMaker columnName(String columnName) {
        this.columnName = columnName;
        return this;
    }

    public EnumStatusMaker tableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public EnumStatusMaker name(String name) {
        this.name = name;
        className = FreeMakerUtil.firstUpper(name);
        return this;
    }

    public EnumStatusMaker choiceType(String choiceType) {
        this.choiceType = choiceType;
        return this;
    }

    public EnumStatusMaker addValue(String key, EnumData value) {
        values.put(key, value);
        return this;
    }

    public EnumStatusMaker remove(String key) {
        values.remove(key);
        return this;
    }

    @Override
    public EnumStatusMaker make() {
        data();
        out("enum.ftl", data);

        return this;
    }

    public String getPack() {
        return pack;
    }

    public String getChoiceType() {
        return choiceType;
    }

    public Map<String, EnumData> getValues() {
        return this.values;
    }

    public String getClassName() {
       return FreeMakerUtil.firstUpper(name);
    }

    public String getShowName() {
        return FreeMakerUtil.firstUpper(name);
    }

    public String getColumnName() {
        return columnName;
    }

    public String getTableName() {
        return tableName;
    }

    public String toString() {
        data();
        if (values.size() < 1) return "";
        return toString("enum.ftl", data);
    }

    public void data() {
        className = FreeMakerUtil.firstUpper(name);
        check();
        data.put("enumValues", values);
        if (type != null) {
            data.put("type", type.getSimpleName());
        }
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str=sdf.format(new Date());
        data.put("Author",author);
        data.put("DATE",str);
        data.put("Description", description);
        data.put("className", className);
        boolean isString = type != null ? type == String.class : true;
        data.put("isString", isString);
        super.data();
    }
}
