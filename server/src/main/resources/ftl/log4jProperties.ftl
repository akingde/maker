log4j.rootCategory=debug, stdout

log4j.logger.${projectRootPackage}=debug
log4j.logger.org.springframework=warn
log4j.logger.com.mybatis=warn
log4j.logger.org.mybatis.spring=warn
log4j.logger.org.hibernate=warn
log4j.logger.com.mybatis.common.jdbc.SimpleDataSource=debug
log4j.logger.com.mybatis.common.jdbc.ScriptRunner=debug
log4j.logger.com.mybatis.sqlmap.engine.impl.SqlMapClientDelegate=debug
log4j.logger.java.sql.Connection=debug
log4j.logger.java.sql.Statement=debug
log4j.logger.java.sql.PreparedStatement=debug
log4j.logger.java.sql.ResultSet=debug

log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss,SSS} [%t] %-5p %c %L - %m%n