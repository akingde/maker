package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by ldh on 2017/2/26.
 */
public class SQLiteJDBC {

    public static void main( String args[]) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("util:sqlite:test.db");
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
//            String sql = "CREATE TABLE COMPANY " +
//                    "(ID INT PRIMARY KEY     NOT NULL," +
//                    " NAME           TEXT    NOT NULL, " +
//                    " AGE            INT     NOT NULL, " +
//                    " ADDRESS        CHAR(50), " +
//                    " SALARY         REAL)";
            String sql0 = "insert into company(id, name, age, address, salary) values(2, 'test', 28, '', null)";
            stmt.executeUpdate(sql0);
            String sql = "select count(*) from COMPANY";
            ResultSet set = stmt.executeQuery(sql);
            if (set.next()) {
                Long count = set.getLong(1);
                System.out.println("Table created successfully" + count);
            }
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

    }
}
