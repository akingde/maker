import 'package:flutter/material.dart';
import 'dart:async';

class InitPane extends StatelessWidget {
  InitPane({Key key, this.initWidget, this.child, this.isShowInit}) : super(key: key) {
    if (initWidget == null) {
      initWidget = const CircularProgressIndicator();
    }
  }

  Widget initWidget;
  Widget child;
  bool isShowInit = true;

  @override
  Widget build(BuildContext context) {
    if (isShowInit) {
      return new Center(
        child: initWidget,
      );
    }
    return child;
  }

}